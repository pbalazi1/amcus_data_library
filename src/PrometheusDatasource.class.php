<?php
// Redis singleton class. Create on 2020-10-05, by petr.balazi@skamanet cz
class PrometheusDatasource
{
    private $_connection;
    private static $_instance; //The single instance
    private $_host = DB_HOST;
    private $_password = DB_USER;

    /*
    Get an instance of the Redis connection
    @return Instance
    */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Constructor
    private function __construct()
    {
        try {
            // Connect to redis - TODO
            $this->_connection  = NULL;
        } catch (PDOException $e) {
            echo $e->getMessage();
            $this->connection = NULL;
        }
    }

    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {
    }

    // Get mysql pdo connection
    public function getConnection()
    {
        return $this->_connection;
    }
}
?>