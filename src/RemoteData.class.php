<?php

namespace Amcus2\RemoteData;

use RedisDatasource;
use PrometheusDatasource;

class RemoteDataClass
{
    // private variables
    private $_kv_connection;
    private $_history_v_connection;
    private $_demo_signals = array('2101_SCHR_UPS1_2026' => 50, '2101_SCHR_UPS1_2031' => 230, '2101_SCHR_UPS1_2032' => 230, '2101_SCHR_UPS1_2033' => 230);
    /* demo signals
        2101_SCHR_UPS1_2026 - IN-HZ
        2101_SCHR_UPS1_2031 - OUT-U-L1
        2101_SCHR_UPS1_2032 - OUT-U-L2
        2101_SCHR_UPS1_2033 - OUT-U-L3
    */

    /**
     * Create a new RemoteData Instance
     */
    public function __construct()
    {
        $_kv_connection = RedisDataSource::getInstance();
        if (is_null($_kv_connection)) {
            throw new Exception( 'Connection to Key-Value store failed!' );
        }
        $_history_v_connection = PrometheusDataSource::getInstance();
        if (is_null($_history_v_connection)) {
            throw new Exception( 'Connection to Historic data failed!' );
        }
    }

    // Private methods
    
    /**
     * Detect if Data source exists in DB
     * @param string $signalId - id of datapoint which of value should be returned
     * @return boolean - true if signal is known in databse
     */     
    private function _dataPointExists($dataPointId)
    {
        $retVal = FALSE;
        # in case it exists, return true
        if (array_key_exists($dataPointId, $this->_demo_signals))
        {
           $retVal = TRUE;
        }
        // return value
        return $retVal; 
    }

    private function _dataPointOffset($dataPointId)
    {
        $retVal = PHP_INT_MAX;

        if($this->_dataPointExists($dataPointId))
        {
            # DEMO
            $retVal =  20 
        }
        // return value
        return $retVal;
    }


    // Public methods


    /**
     * Get current value from internal DB for exact datapoint
     * @param string $signalId - id of datapoint which of value should be returned
     * @param number $allowedOffset - allowed offset of time - how old value can be and still considered as current
     * @return number - returns the current value
     */
    public function getCurrentValue($dataPointId, $allowedOffset = 300)
    {
        $retVal = NULL;

        # DEMO mode, check if we knos signal in demo mode
        if ($this->_dataPointExists($dataPointId))
            $retVal $this->_demo_signals[$dataPointId];
        else {
            throw new Exception ("Data point ".$dataPointId." does not exists.");
            return NULL;
        }
        # detect not valid offset for data point
        $dataPointOffset = $this->_dataPointOffset($dataPointId)
        if ($dataPointOffset > $allowedOffset)
        {
            throw new Exception ("Data point ".$dataPointId." is not recent enought (".$dataPointOffset.").");
            return NULL;
        }
  
    }

    /**
     * Get signal details
     * @param string $signalId - id of datapoint which of value should be returned
     * @return number - returns associative array with details
     */
    public function getDataPointDetails($dataPointId)
    {
        # DEMO mode, check if we knos signal in demo mode
        if (_dataPointExists($dataPointId))
            return array("name" => $dataPointId,"help" => "Signal description","type" => "gauge","labelNames" => ["datatype","devicebox"]);
        else {
            throw new Exception ("Data point ".$dataPointId." does not exists.");
            return NULL;
        }
    }  

    /**
     * Get history values
     * @param string $signalId - id of datapoint which of value should be returned
     * @param timestamp $timestampFrom - timestamp from which date time point data should be provided
     * @return number - returns array with numbers
     */
    public function getHistoryValues($dataPointId, $timestampFrom)
    {
        $retVal = array(50,49,50,52,50,51,52,51);
        return($retVal);
    }
}
